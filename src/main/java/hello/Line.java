package hello;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Represents a line in an order with a SKU and a quantity
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
public class Line {
	
	private long sku;
	private int quantity;
	
	/**
	 * Creates an order line
	 * @param sku the product's SKU
	 * @param quantity the quantity ordered
	 */
	public Line (long sku, int quantity) {
		this.sku = sku;
		this.quantity = quantity;
	}
	
	/**
	 * Get the SKU of the product on the line
	 * @return the SKU of the produt
	 */
	public long getSku() {
		return sku;
	}
	
	/**
	 * Get the quantity of the product ordered
	 * @return the quantity ordered
	 */
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * Set the quantity of the product ordered
	 * @param quantity the quantity ordered
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
